import deploy, logger, sys, config_reader
from subprocess import run, PIPE
from os import getcwd, path, remove

"""
    Class responsible for multiple actions of config data
    Uses logger.Logger to write log into memory and file
"""
class DeployerActioner:
    __deploy_config = None
    __logger = None
    __config_reader = None

    __DEFAULT_FILES = {"ddl.sql", "dml.sql", "pv.sql", "preinstall.sql"}
    __PATCH_PATH = ".\\sql"
    #md5 for message succesfully installed #$^
    __SUCCESS_MESSAGE = "BD4F3F54E9EF849F6998300346DF3433"

    # constructor
    def __init__(self, dc):
        self.__deploy_config = dc
        self.__logger = logger.Logger() # default logger
        self.__config_reader = config_reader.ConfigReader(self.dc.server)

    # deploy config object
    @property
    def dc(self):
        return self.__deploy_config
    
    #confir_reader object
    @property
    def cr(self):
        return self.__config_reader

    # logger object
    @property
    def mylogger(self):
        return self.__logger
    

    # function prompts for new values for some data
    # param d is dictionary where key is data's description, and value is data's value
    def prompt_for_input(self, d):
        l = []
        for key in d:
            if d[key] is None:
                v = input('Please input value for "{}" parameter > '.format(key))
                if v == "":
                    raise Exception("Missing value: {}".format(key))
                l.append(v)
            else:
                v = input('Readed value "{}" for "{}" from config.\n If you want to replace, please enter new value> '.format(d[key], key))
                if v == "":
                    l.append(d[key])
                else:
                    l.append(v)
        return tuple(l)

    # function to copy file
    def copyfile(self, from_, to):
        self.mylogger.log("looking for file {}".format(from_))
        if path.exists(from_):
            with open(from_, 'r', encoding="utf-8-sig") as f:
                with open(to, mode="a") as wf:
                    wf.write(f.read() + "\n")
            self.mylogger.log("Succesfully copied into {}".format(path.abspath(to)))
        else:
            self.mylogger.log("File {} not found in Directory {}".format(path.basename(from_), path.abspath(path.dirname(from_)) ))
    
    # one piece processing
    def process_patch(self, patch):
        patch_path = path.join(".\\Patches", patch)
        self.mylogger.log("Looking up for patch {}".format(patch_path))

        if path.exists(patch_path):
            self.mylogger.log("Looking for files in patch {}".format(patch))
            
            for file in self.__DEFAULT_FILES:
                to_patch = path.join(self.__PATCH_PATH, file)
                self.copyfile(path.join(patch_path, file), to_patch)
        else:
            self.mylogger.log_error("Not found patch '{}' at path '{}'".format(patch_path, path.abspath(patch_path)))
    
    # checks default patch files located in __patch_path and create not existed
    def create_default_files(self, truncate = False):
        self.mylogger.log("Start processing default files")

        for file in self.__DEFAULT_FILES:
            path_ = path.join(self.__PATCH_PATH, file)
            if not path.exists(path_):
                with open(path_, 'w', encoding="utf-8-sig"):
                    pass
                self.mylogger.log("File {} not finded at {}. Created".format(file, path_))
            else:
                if truncate:
                    with open(path_, 'w', encoding="utf-8-sig"):
                        pass
                    self.mylogger.log("File {} created truncated".format(file))
                else:
                    self.mylogger.log("File {} finded and uses".format(file))
        self.mylogger.log("End processing default files")
    
    # create common patch from patches
    def create_by_patches(self):
        self.mylogger.log("Start of processing patches")

        self.create_default_files(True)

        for patch in self.dc.issues:
            self.process_patch(patch)
        self.mylogger.log("End of processing patches")
    
    #creates sql file to run install.patch
    def execute_sql_patch(self):
        
        # read ora params
        user, pas, server = self.cr.read_ora_params()

        # try to read ut_release_params
        release_num, release_comment, is_fix, vcs_branch = self.cr.read_ut_release_params()
        d = {
                "release num": release_num, 
                "release comment": release_comment, 
                "is fix or standalone release (0/1)": is_fix, 
                "version control system branch": vcs_branch
            }
        release_num, release_comment, is_fix, vcs_branch = self.prompt_for_input(d)

        # create sql file
        with open(".\\sql\\run_sql.bat", 'w', encoding="cp1251") as f:
            f.write("@echo off\nchcp 65001\n")
            f.write(
                'sqlplus -s {user}@{server}/{passwd} @install_patch.sql "{num}" "{comment}" "{ver}" "{hgbranch}"\n'
                .format(
                    user=user, 
                    server=server, 
                    passwd=pas, 
                    num=release_num,
                    comment=release_comment,
                    ver=is_fix,
                    hgbranch=vcs_branch
                )
            )
            f.write("exit 0")

        #sql log file
        slf = path.abspath(".\\sql\\install.log")
        with open(slf, 'w', encoding="utf-8") as f:
            r = run(["run_sql.bat"], shell=True, cwd=path.join(getcwd(), "sql"), stdout=f, stdin=PIPE)
        
        with open(slf, 'r', encoding="utf-8") as f:
            s = f.read()
            if self.__SUCCESS_MESSAGE in s:
                self.mylogger.log(s)
            else:
                self.mylogger.log_error(s)
        # delete slf
        remove(slf)

    # initialization
    def init(self):
        with open(self.mylogger.file, 'w', encoding="utf-8-sig"):
            pass
        try:
            remove(".\\sql\\install.log")
        except EnvironmentError:
                pass

    # main action
    def run(self):
        try:
            # initializing
            self.init()
            # if no server = error
            if self.dc.server is None:
                self.mylogger.log_error("Cannot continue - no ora server specified")
                raise EnvironmentError("Cannot continue - no ora server specified")
            # if no patches - process default files
            if len(self.dc.issues) == 0:
                self.mylogger.log("No issues specified by -i param => run with default files located in cwd")
                self.create_default_files()
            else:
                self.mylogger.log("Finded issues. Process them")
                self.create_by_patches()
            
            #create bat file which run sqlplus and log all sql commands to log file
            # after execution we will check operation
            self.execute_sql_patch()
            
            if self.mylogger.has_errors:
                print("Errors finded. Please see log file {}".format(path.abspath(self.mylogger.file)))
            else:
                print("No errors finded. Seem to be deployed succesfully. But please see log file {}".format(path.abspath(self.mylogger.file)))
        finally:
            self.mylogger.flush_log()
    
    def log_file(self):
        return path.abspath(self.mylogger.file)
