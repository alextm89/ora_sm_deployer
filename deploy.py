"""
This script installs oracle patch. Patch is 4 files with names DDL.sql, DML.sql, PV.sql, PreInstall.sql.
Script is command-line utility that can have some arguments.
Argumets processed by argparse unit
"""
import argparse, os, actioner, sys, configparser

"""
    Deploy config class. Read application config from command line or config files
"""
class DeployArgumentConfig:
    __issues = []   # issues list, each as folder with files
    __server = ""   # oracle server
    __show_help = False


    def show_help():
        with open("install_info.md", "r", encoding="utf-8") as f:
            s = f.read();
            print(s)
        return

    def __init__(self, server, issues):
        self.__issues = issues
        self.__server = server

    @property
    def server(self):
        return self.__server
    
    @server.setter
    def server(self, x):
        self.__server = x
    
    @property
    def issues(self):
        if self.__issues is None:
            return list()
        else:
            return self.__issues
    
    @issues.setter
    def issues(self, x):
        self.__issues = x

def main():
    ap = argparse.ArgumentParser(prog='ora_deployer', description='Used to deploy oracle changes (DDL, DML) to different enviroments')
    # register params
    ap.add_argument('-s', '--server', nargs = '?', type=str, 
        help="Oracle server used to install changes.")
    ap.add_argument('-i', '--issues', nargs = '*', type=str, 
        help="Jira issues list comma or space separated, used to create common changeset to install it.")
    ap.add_argument("--utr_num", nargs='?', type=str,
        help="UT release install num. Overrides config value")
    ap.add_argument("--utr_comment", nargs='?', type=str,
        help="UT release install comment")
    ap.add_argument("--utr_is_fix", nargs='?', type=str,
        help="UT release param: is this patch fix or standard release (0/1) ?")
    ap.add_argument("--utr_branch", nargs='?', type=str,
        help="UT release param: version control current branch")
    ap.add_argument("--show-help", nargs='?', type=bool, default=False,
        help="Show main program help")

    # get param values as map[long_name as key, list or single value as value]
    parsed = vars(ap.parse_args())
    # calculates of show_help parameter
    show_help = parsed['show_help'] or (parsed['show_help'] is None)
    if show_help:
        DeployArgumentConfig.show_help()
        return
    # puts it into config object
    dc = DeployArgumentConfig(parsed['server'], parsed['issues'])


    a = actioner.DeployerActioner(dc)
    try:
        a.run()
    except:
        print("Critical errors finded... So sorry. Please see log file {}".format(a.log_file() ))
        

if __name__ == "__main__":
    main()