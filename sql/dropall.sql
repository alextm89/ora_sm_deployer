declare
    v_dropped boolean := true;
    v_times   pls_integer := 0;
    v_error   varchar2(32000 char) := '';

    function drop_all return boolean
    as
    begin
        -- пробежимся по всем объектам, по приоритетам их расставим
        for rec in 
        (
            select
                o.object_name,
                o.object_type
            from
                user_objects o
            where
                o.object_type not in
                    (
                        'INDEX',
                        'LOB'
                    )
            order by
                decode
                (
                    o.object_type,
                    'DATABASE LINK', 1,
                    'JOB', 2,
                    'SEQUENCE', 3,
                    'SYNONYM', 4,
                    'TRIGGER', 5,
                    'TYPE', 6,
                    'PACKAGE', 7,
                    'TABLE', 9
                )
        ) 
        loop
            if rec.object_type = 'JOB' then
                dbms_scheduler.drop_job(rec.object_name);
            else
                execute immediate 'drop ' || rec.object_type || ' ' || rec.object_name || 
                    case when rec.object_type = 'TABLE' then ' cascade constraints' end ||
                    case when rec.object_type = 'TYPE' then ' force' end;
            end if;
        end loop;
        return true;
    exception
        when others then
            v_error := v_error || sqlerrm || chr(10);
            return false;
    end;
begin
    -- чистим корзину
    execute immediate 'purge recyclebin';
    v_dropped := drop_all();
    while not v_dropped or v_times < 3
    loop
        v_times := v_times + 1;
        v_dropped := drop_all();
    end loop;
    -- еще раз чистим корзину
    execute immediate 'purge recyclebin';
    if not v_dropped then
        ut.ut_utils.myerror(v_error);
    end if;
end;
/
exit 0