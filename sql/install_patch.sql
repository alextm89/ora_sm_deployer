prompt starting execute oracle ...
prompt set up sqlplus 
whenever sqlerror exit failure
whenever oserror exit failure
set echo on
set termout on
set define on
set trimspool on
set wrap on
set sqlblanklines on
set heading on
set linesize 500
set pagesize 1000
set verify off
set feed on
set autoprint off
set arraysize 5000

prompt set release metadata
var v_sid number;
var v_release_num varchar2(100 char);
var v_ver_comment varchar2(4000 char);
var v_release_type number;
var v_hg_branch varchar2(4000 char);

declare
    v_finded boolean := false;
begin
    -- номер релиза всегда берется из конфига - его параметром забираем
    :v_release_num  := '&1';
    -- новая логика - если есть незакрытый релиз с датой начала менее 10 минут назад, то берем параметры с него
    for rec in 
        (
            select
                l.sid,
                l.remark,
                l.mercurial_branch
            from
                ut.ut_release_log l
            where
                l.owner = user
                and l.dbuser = user
                and numtodsinterval(sysdate - l.beg_dat, 'day') <= interval '10' minute
                and l.end_dat is null
                and l.num = :v_release_num
        )
    loop
        :v_sid          := rec.sid;
        :v_ver_comment  := rec.remark;
        :v_hg_branch    := rec.mercurial_branch;
        v_finded := true;
    end loop;
    -- не нашли - заново устанавливаем
    if not v_finded then
        :v_sid          := userenv('sid');
        :v_ver_comment  := '&2';
        :v_release_type := &3;
        :v_hg_branch    := '&4';
    end if;
end;
/

begin
    ut.ut_release.install
    (
        p_num              => :v_release_num,
        p_mercurial_branch => :v_hg_branch,
        p_type             => :v_release_type,
        p_remark           => :v_ver_comment
    );
end;
/
set define off

prompt run preinstall.sql
@@preinstall.sql

prompt start installing
exec prs_release_utils.start_install_release;

prompt DDL.sql....................................
@@DDL.sql
prompt 'Views, Packages sql.................................'
@@PV.sql

prompt recompiling schema and searching for Invalids
--перекомилим схему
-- если нет инвалидов - запускаем dml
begin
    ut.ut_compile_invalid_objects(p_schema => user);
    for rec in (select * from user_objects o where o.status = 'INVALID') loop
        ut.ut_utils.myerror('Invalid objects found - cannot start DML');
    end loop;
end;
/
prompt after applying all ddl sets end of release installing
begin
    ut.ut_release.finish(:v_sid);
end;
/

prompt update data
whenever sqlerror exit failure rollback

prompt DML...............................
@DML.sql

prompt finalizing release install
exec prs_release_utils.stop_install_release;

prompt commit
commit;

-- hash md5 for string "succesfully installed #$^"
prompt write hash for succesfull operation into log
prompt BD4F3F54E9EF849F6998300346DF3433
prompt end of oracle executing
exit