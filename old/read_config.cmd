@setlocal enableextensions enabledelayedexpansion
@echo off
for /f "delims=^ tokens=1" %%a in (config.cfg) do (
    set ff=%%a
    
    rem первый и последний символ
    set b1=!ff:~0,1!
    set b2=!ff:~-1!
    rem читаем название секции
    if "!b1!!b2!" == "[]" ( 
        set section=!ff:~1,-1!
    ) else (
        for /f "delims== tokens=1,2" %%l in ("!ff!") do (
            if "!section!" == "%1" (
                if "%%l" == "username" ( set login=%%m)
                if "%%l" == "password" ( set pass=%%m)
                if "%%l" == "server" ( set server=%%m)
            )
            rem параметры релиза
            if "!section!" == "release" (
                if "%%l" == "num" (set num=%%m)
            )
        )
    )
)
echo release=%num%
echo login=%login%
echo password=%pass%
echo server=%server%
@endlocal