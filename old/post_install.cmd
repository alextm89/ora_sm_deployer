echo off
setlocal enableextensions enabledelayedexpansion

rem Форматируем дату
set yyyy=%date:~-4%
set mm=%date:~3,2%
set dd=%date:~0,2%

set curdate=%yyyy%-%mm%-%dd%
echo Formatted curdate: %curdate% >> install.log
echo. >> install.log

rem читаем tmp файл с нужными нам параметрами
if not exist install.tmp (
	echo "No install.tmp. Finish." >> install.log
	goto :finish
)
set n=0
for /f "delims=^ tokens=1" %%a in (install.tmp) do (
	set /a n = !n! + 1
	if !n! equ 1 (set isok=%%a)
	if !n! equ 2 (set server=%%a)
)
echo Read params from install.tmp: isok =%isok%, server= %server%, num =%num% >> install.log
echo. >> install.log

if not %isok% equ ok (goto :finish)

rem читаем параметры подключения к серверу
if not exist config.cfg (
	echo "No config.cfg" >> install.log
	goto :finish
)

for /f "delims== tokens=1,2 usebackq" %%a in (`read_config.cmd %server%`) do (
    if %%a equ login (set login=%%b)
    if %%a equ password (set pass=%%b)
    if %%a equ server (set server=%%b)
    if %%a equ release (set num=%%b)
)
echo Readed params from config: login=%login%, password=*********, server = %server%, release_num = %num% >> install.log
echo. >> install.log

rem выполняем sql скрипт, в котором снимаем блокировку с системы
echo exec prs_release_utils.stop_install_release; > run.sql
echo exit 0 >> run.sql

sqlplus -s %login%/%pass%@%server% @run.sql

rem посчитаем кол-во раз, которое встретится папка с текущей датой среди установленных ранее
set count=0
for /d /r %%g in ("INSTALLED\%num%\%curdate%*") do set /a count = !count! + 1
echo Finded %count% of early installed with date %curdate% >> install.log

rem задаем dirpath
if %count% equ 0 ( set count=) else (set count=_%count%)
set dirpath=INSTALLED\%num%\%curdate%%count%\
echo Calculated dirpath: %dirpath% >> install.log

rem копируем установленные файлы в патч
if not exist "%dirpath%" ( mkdir %dirpath% )

for %%f in (DML.sql,DDL.sql,PV.sql,PreInstall.sql, install.log) do (
move .\%%f %dirpath%
)
rem удаляем временные файлы
erase run.sql /q
erase install.tmp /q

:finish
endlocal
