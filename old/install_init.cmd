@echo off
rem используется для начальной установки приложения
rem меняем кодировку на UTF-8
chcp 65001

rem модифицируем переменные среды только внутри сессии
@setlocal enableextensions enabledelayedexpansion
set /p constring="Enter connect string in format user/pass@server> "
echo Dropping schema data ...
call sqlplus %constring% @dropall.sql
echo install
cd ..
call sqlplus %constring% @awake.sql
cd Deploy
for /f "delims=* usebackq" %%a in (`call sqlplus -s %constring% @can_run_tests.sql`) do (
    if "%%a" equ "true" (
        cd ..
        echo installing tests
        call sqlplus -s %constring% @awake_tests.sql
    ) else (
        echo no tests needed
    )
)
@endlocal