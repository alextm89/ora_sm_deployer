set serveroutput on
set verify off
set feed off
begin
	if prs_ddl_utils.current_server_is_prod() then
		dbms_output.put_line('false');
	else
		dbms_output.put_line('true');
	end if;
end;
/
exit 0