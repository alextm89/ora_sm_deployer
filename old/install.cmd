@echo off
rem Меняем кодировку на UTF-8
chcp 65001

echo Installing release begin .. > install.log

rem удаляем временные файлы скрипта
echo delete temporary files .. >> install.log
for %%F in (run.sql, install.tmp) do (
    if exist "%%F" (
        erase /q %%F
        echo %%F erased >> install.log
    ) else (
        echo file %%F not exist >> install.log
    )
)

@setlocal enableextensions enabledelayedexpansion
rem пересоздаем файлы, нужные скрипту установки патча для СУБД, если указали патчи через командную строку
rem если не указали - используем имеющиеся
if not "%2" equ "" (
    echo Create empty files for release builder ... >> install.log
    for %%F IN (DDL.sql DML.sql PV.sql PreInstall.sql) do ( 
        echo( > %%F
        echo Created %%F >> install.log
    )
)
echo. >> install.log

rem читаем конфиг -скрипт чтения возвращает строки вида name=value
set conf_serv=%1
for /f "delims== tokens=1,2 usebackq" %%a in (`read_config.cmd %1`) do (
    if %%a equ login (set login=%%b)
    if %%a equ password (set pass=%%b)
    if %%a equ server (set server=%%b)
    if %%a equ release (set num=%%b)
)
echo Readed params from config: login=%login%, password=*********, server = %server%, release_num = %num% >> install.log
echo. >> install.log

rem фомируем текст файлов для работы билдера СУБД
rem Набор патчей передается в скрипт, начиная со второго параметра командной строки
:do
    if "%2" equ "" (goto :exit) else (goto :body)
    :body
    for %%f IN (DDL.sql DML.sql PV.sql PreInstall.sql) do (
        set pth=Patches\%2\%%f
        if exist "!pth!" ( 
            type !pth! >> %%f
            echo. >> %%f
            echo !pth! copied >> install.log
        ) else ( echo !pth! not exist >> install.log)
    )
    shift /1
    goto :do
:exit
rem запрашиваем у пользователя информацию о метаданных релиза
set release_type=0
set hgbranch=master
set /p comment="Enter release comment> "
set /p release_type="Enter release type (0 - release, 1 - hotfix, defaul 0) >"
set /p hgbranch="Enter current mercurial branch (default master)> "

echo Getting ut release params from user input: release_type = %release_type%, hgbranch = %hgbranch%, comment = %comment% >> install.log
echo. >> install.log

rem формируем файл запуска установщика скриптов

rem в нем две строки
set l1=start install_patch "%num%" "%comment%" "%release_type%" "%hgbranch%"> run.sql
set l2=exit 0

echo %l1% > run.sql
echo %l2% >> run.sql

echo %l1% >> install.log
echo %l2% >> install.log

rem запуск скрипта
echo installing ...
sqlplus %login%/%pass%@%server% @run.sql
echo installing end

rem проверим ошибки
rem sql скрипт пишет в конце сообщение об успешности "succesfully installed #$^"
for /f "delims=: tokens=1,2 usebackq" %%a in (`find /c "succesfully installed #$^" install.log`) do (set noerrors=%%b)

if  not "%noerrors%" equ " 0" ( 
    rem запишем сервер, куда успешно установили релиз, ведь приложение осталось заблокированным для пользователей
    echo ok > install.tmp
    echo %conf_serv% >> install.tmp
    rem вызов скрипта окончания
    post_install.cmd
    echo no errors finded
) else (
    echo Finded errors. see install.log for details
    echo false > install.tmp
    rem выход
    exit 1 
)

@endlocal