﻿set termout off
spool install.log append
set define on
set trimspool on
set wrap off
set sqlblanklines on
set heading on
set linesize 500
set pagesize 1000
set echo on
set verify off
set feed on
set autoprint off
set arraysize 5000
whenever sqlerror exit failure
whenever oserror exit failure

prompt release metadata
var v_sid number;
var v_release_num varchar2(100 char);
var v_ver_comment varchar2(4000 char);
var v_release_type number;
var v_hg_branch varchar2(4000 char);

declare
    v_finded boolean := false;
begin
    -- номер релиза всегда берется из конфига - его параметром забираем
    :v_release_num  := '&1';
    -- новая логика - если есть незакрытый релиз с датой начала менее 10 минут назад, то берем параметры с него
    for rec in 
        (
            select
                l.sid,
                l.remark,
                l.mercurial_branch
            from
                ut.ut_release_log l
            where
                l.owner = user
                and l.dbuser = user
                and numtodsinterval(sysdate - l.beg_dat, 'day') <= interval '10' minute
                and l.end_dat is null
                and l.num = :v_release_num
        )
    loop
        :v_sid          := rec.sid;
        :v_ver_comment  := rec.remark;
        :v_hg_branch    := rec.mercurial_branch;
        v_finded := true;
    end loop;
    -- не нашли - заново устанавливаем
    if not v_finded then
        :v_sid          := userenv('sid');
        :v_ver_comment  := '&2';
        :v_release_type := &3;
        :v_hg_branch    := '&4';
    end if;
end;
/

begin
    ut.ut_release.install
    (
        p_num              => :v_release_num,
        p_mercurial_branch => :v_hg_branch,
        p_type             => :v_release_type,
        p_remark           => :v_ver_comment
    );
end;
/
set define off
prompt preinstall
@preinstall.sql

prompt start installing
exec prs_ddl_utils.start_install_release;

prompt kill blocking users
-- call top_ddl_utils.kill_blocking_users();

prompt DDL....................................
@DDL.sql
prompt 'Views, Packages...............................'
@PV.sql
--перекомилим схему
-- если нет инвалидов - запускаем dml
begin
    ut.ut_compile_invalid_objects(p_schema => user);
    for rec in (select * from user_objects o where o.status = 'INVALID') loop
        ut.ut_utils.myerror('Invalid objects found - cannot start DML');
    end loop;
end;
/
prompt after applying all ddl sets end of release installing
begin
    ut.ut_release.finish(:v_sid);
end;
/

prompt update data
whenever sqlerror exit failure rollback

prompt DML...............................
@DML.sql
prompt commit
commit;

prompt succesfully installed #$^
spool off