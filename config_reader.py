#-*-coding:utf-8-*-

import configparser, logger
from os import path

"""
    Class used to read confile of Oracle
"""
class ConfigReader:
    __default_config_file = "config.cfg"
    __config_file = ""
    # section for oracle settings (user, pass, server). usually is short server name like "dev" "prod" etc
    __ora_section = ""
    #default oracle attributes
    __ora_attrs = ("user", "password", "server")

    # section for release info data
    __release_info = "release_info"

    # logger
    __logger = None

    # config parser
    __config_parser = None

    # config parser cf propery
    @property
    def cf(self):
        return self.__config_parser
    @cf.setter
    def cf(self, value):
        self.__config_parser = value
        return
    
    #ora section orasect readonly property
    @property
    def orasect(self):
        return self.__ora_section

    # configuration file confile property
    @property
    def confile(self):
        return self.__config_file

    @confile.setter
    def confile(self, value):
        self.__config_file = value
        return
    
    # logger readonly log property
    @property
    def log(self):
        return self.__logger

    # read params from config file
    def read_params_from_file(self):
        if path.exists(self.confile):
            self.cf.read(self.confile)
            self.log.log("readed config file {}".format(path.abspath(self.confile)))
        else:
            s = "Log file not found at {}".format(cfp)
            self.log.log_error(s)
            raise EnvironmentError(s)

    # constructor gets ora_secton name, release_section name config file name, log file name
    def __init__(self, ora_section, release_section = None, file = None, logfile = None):
        if file == None:
            self.confile = self.__default_config_file
        else:
            self.confile = file
        
        if logfile == None:
            self.__logger = logger.Logger()
        else:
            self.__logger = logger.Logger(log_file)
        self.__ora_section = ora_section

        if release_section != None:
            self.__release_info = release_section
        
        self.cf = configparser.ConfigParser()
        self.read_params_from_file()
    
    # readed ora connection params (user, password, server) and returns it as tuple
    def read_ora_params(self):
        cfp = path.abspath(self.confile)

        # check ora config section
        if self.orasect in self.cf:
            l = []
            for attr in self.__ora_attrs:
                b = attr in self.cf[self.orasect]
                if not b:
                    self.log.log_error("Cannot find value {} in config file '{}' in section [{}]".format(attr, cfp, self.orasect))
                    l.append(attr)
            if len(l) > 0:
                raise EnvironmentError("cannot find values {} in config file '{}' section [{}]".format(l, cfp, self.orasect))
                
            # user, pass, server
            self.log.log(
                "Readed params from file {}: user = {}, password = *******, server = {}"
                .format(
                    cfp,
                    self.cf[self.orasect]["user"],
                    self.cf[self.orasect]["password"]  
                )
            )
            return self.cf[self.orasect].values()
        else:
            s = "Oracle config section [{}] not found in file {} ".format(self.orasect, cfp)
            self.log.log(s)
            raise EnvironmentError("Cannot read oracle settings. {}".format(s))
    
    # readed ut release params (release_num, release_comment, is_fix and vcs_branch) and returns it as tuple
    def read_ut_release_params(self):
        cfp = path.abspath(self.confile)
        # check ut_release section is exists
        if self.__release_info in self.cf:
            attrs = ("num", "comment", "is_fix", "vcs_branch")
            l = []
            for attr in attrs:
                if attr not in self.cf[self.__release_info]:
                    self.log.log("Attribute {} not found in section [{}] of config file {}".format(attr, self.__release_info, cfp))
                    l.append(None)
                else:
                    l.append(self.cf[self.__release_info][attr])
            return tuple(l)
        else:
            self.log.log("UT release section [{}] not found in file {}".format(self.__release_info, cfp))