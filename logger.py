#-*-coding:utf-8-*-
import time


# simple logger class
class Logger:
    #default log file
    __FILE = "install.log"
    
    __log_file = ""
    __log_data = list()
    __has_errors = False

    def __init__(self, log_file = None):
        if log_file == None:
            self.file = self.__FILE
        else:
            self.file = log_file

    def write_error(self):
        self.log("Some errors finded")
    
    def log(self, text):
        self.log_data.append(time.strftime("[ %d.%m.%Y %H:%M:%S ]") + " " + text + "\n")
    
    @property
    def file(self):
        return self.__log_file
    
    @file.setter
    def file(self, value):
        self.__log_file = value
        return
    
    @property
    def log_data(self):
        return self.__log_data
    
    @property
    def has_errors(self):
        return self.__has_errors
    
    @has_errors.setter
    def has_errors(self, value):
        self.__has_errors = value
        return

    def log_error(self, text):
        self.log(text)
        self.__has_errors = True

    def flush_log(self):
        if self.__has_errors:
            self.write_error()
        with open(self.__log_file, 'w', encoding="utf-8") as f:
            f.writelines(self.log_data)