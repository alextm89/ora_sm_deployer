Short info:
This program has written with Python 3.8.*

This program deploy changes (called patches) to oracle servers.
Mandatory params for deploy, such ut_release, executes also.
Program has config and appropriate command line params to deploy changes. 
It writes log, automatically run 

Consists of some python scripts, SQL folder with oracle utilites for deploy, and old folder with old version deploy system, written with windows command prompt.

To get command line help, run main script with help option: 
    .\deploy.py --help
Main help (in Russian) stored in install_info.md file, and can be shown with .\deploy.py --show-help command

NOTE: This program uses proprietary oracle functions from UT package, written by DSKS department of SM lab Oracle team.
        if you want to use it, you should realize it by yourself